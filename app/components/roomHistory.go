package components

import (
    "github.com/gopherjs/vecty"
    "github.com/gopherjs/vecty/elem"
    "gitlab.com/tsuchina.ga/gonsen"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/store"
    "github.com/gopherjs/vecty/event"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/dispatcher"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/actions"
)

type RoomHistoryComponent struct {
    vecty.Core
}

func (c *RoomHistoryComponent) Render() (body vecty.ComponentOrHTML) {
    var list vecty.List
    for _, roomId := range store.RoomHistory {
        id := roomId
        if id != "" {
            list = append(list, ons.ListItem(vecty.Text(id), vecty.Markup(
                vecty.Attribute("modifier", "chevron"),
                vecty.Attribute("tappable", true),
                event.Click(func(_ *vecty.Event) {
                    dispatcher.Dispatch(actions.EnterRoom{RoomId: id})
                }))))
        }
    }

    if len(list) == 0 {
        list = append(list, ons.ListItem(vecty.Text("No History")))
    }

    body = elem.Div(
        ons.ListTitle(vecty.Text("room history")),
        ons.List(list))
    return
}
