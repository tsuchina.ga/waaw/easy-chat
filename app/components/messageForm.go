package components

import (
    "github.com/gopherjs/vecty"
    "gitlab.com/tsuchina.ga/gonsen"
    "github.com/gopherjs/vecty/event"
    "net"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/dispatcher"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/actions"
    "github.com/gopherjs/gopherjs/js"
)

type MessageFormComponent struct {
    vecty.Core
    Conn net.Conn
    message string
}

func (c *MessageFormComponent) Render() (body vecty.ComponentOrHTML) {
    body = ons.BottomToolbar(
        vecty.Markup(vecty.Style("text-align", "center")),
        ons.Input(vecty.Markup(
            vecty.Style("width", "calc(100% - 100px)"),
            vecty.Attribute("modifier", "underbar"),
            vecty.Attribute("id", "message"),
            event.Input(func(e *vecty.Event) {
                c.message = e.Target.Get("value").String()
            }),
            event.KeyUp(func(e *vecty.Event) {
                if e.Get("keyCode").String() == "13" { // enter
                    c.submit()
                }
            }))),
        ons.Button(vecty.Text("Send"), vecty.Markup(
            event.Click(func(_ *vecty.Event) {
                c.submit()
            }))))
    return
}

func (c *MessageFormComponent) submit() {
    dispatcher.Dispatch(actions.SendMessage{Message: c.message, Conn: c.Conn})
    js.Global.Get("document").Call("getElementById", "message").Set("value", "")
    c.message = ""
}
