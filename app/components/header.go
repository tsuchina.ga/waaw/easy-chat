package components

import (
    "github.com/gopherjs/vecty"
    "github.com/gopherjs/vecty/elem"
    "github.com/gopherjs/vecty/event"
    "github.com/gopherjs/gopherjs/js"
    "gitlab.com/tsuchina.ga/gonsen"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/dispatcher"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/actions"
)

type HeaderComponents struct {
    vecty.Core
    Title string
    IsBackAble bool
}

func (c *HeaderComponents) Render() (body vecty.ComponentOrHTML) {
    if c.Title == "" {
        c.Title = "easy-chat"
    }

    var backButton vecty.ComponentOrHTML
    if c.IsBackAble {
        backButton = ons.ToolbarButton(vecty.Text("Back"), vecty.Markup(event.Click(func(_ *vecty.Event) {
            dispatcher.Dispatch(actions.ExitRoom{})
        })))
    }

    body = ons.Toolbar(
        elem.Div(vecty.Markup(vecty.Class("left")), backButton),
        elem.Div(vecty.Markup(vecty.Class("center")), vecty.Text("easy-chat")),
        elem.Div(vecty.Markup(vecty.Class("right")), ons.ToolbarButton(
            ons.Icon(vecty.Markup(vecty.Attribute("icon", "ion-social-github"))),
            vecty.Markup(event.Click(func(_ *vecty.Event) {
                js.Global.Get("window").Call("open", "https://gitlab.com/tsuchina.ga/waaw/easy-chat")
            })))))
    return
}
