package store

import (
    "github.com/go-humble/locstor"
    "encoding/json"
)

var (
    UserName = "ななしさん"

    MaxRoomHistory = 10

    RoomHistory = make([]string, MaxRoomHistory)
)

func init() {
    if userName, err := locstor.GetItem("user_name"); err == nil {
        UserName = userName
    }

    if roomHistoryJson, err := locstor.GetItem("room_history"); err == nil {
        var roomHistory []string
        if err := json.Unmarshal([]byte(roomHistoryJson), &roomHistory); err == nil {
            RoomHistory = roomHistory
        }
    }
}
