package store

import "time"

type Message struct {
    UserName string `json:"user_name"`
    Text string `json:"text"`
    CreatedAt time.Time `json:"created_at"`
}
