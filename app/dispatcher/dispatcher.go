package dispatcher

import "gitlab.com/tsuchina.ga/waaw/easy-chat/app/actions"

type ID int

var callbacks []func(action actions.Action)

func Dispatch(action actions.Action) {
    for _, c := range callbacks {
        c(action)
    }
}

func Register(callback func(action actions.Action)) ID {
    id := ID(len(callbacks))
    callbacks = append(callbacks, callback)
    return id
}

func Unregister(id ID) {
    callbacks = callbacks[:int(id)]
    remain := callbacks[int(id):]
    if len(remain) > 1 {
        callbacks = append(callbacks, remain[id+1:]...)
    }
}
