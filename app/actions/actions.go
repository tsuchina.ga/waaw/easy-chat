package actions

import "net"

type EditUserName struct {
    base
    UserName string
}

type EnterRoom struct {
    base
    RoomId string
}

type ExitRoom struct {
    base
    ToastMessage string
}

type SendMessage struct {
    base
    Message string
    Conn net.Conn
}
