package actions

type Action interface {
    action()
}

type base struct {}
func (a base) action() {}
