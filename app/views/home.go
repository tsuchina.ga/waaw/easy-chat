package views

import (
    "github.com/gopherjs/vecty"
    "github.com/gopherjs/vecty/elem"
    "gitlab.com/tsuchina.ga/gonsen"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/components"
    "github.com/gopherjs/vecty/event"
    "strconv"
    "time"
    "encoding/base64"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/dispatcher"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/actions"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/store"
)

type HomeView struct {
    vecty.Core
    UserName string
    roomName string
}

func (v *HomeView) Render() (body vecty.ComponentOrHTML) {
    v.UserName = store.UserName

    body = elem.Body(
    ons.Page(
        &components.HeaderComponents{},
        elem.Div(vecty.Markup(vecty.Style("text-align", "center")),
            elem.Paragraph(ons.Input(vecty.Markup(
                vecty.Attribute("value", v.UserName),
                vecty.Attribute("modifier", "underbar"),
                vecty.Attribute("placeholder", "room name"),
                event.Input(func(e *vecty.Event) {
                    dispatcher.Dispatch(actions.EditUserName{UserName: e.Target.Get("value").String()})
                })))),
            elem.Paragraph(ons.Input(vecty.Markup(
                vecty.Attribute("modifier", "underbar"),
                vecty.Attribute("placeholder", "room name"),
                event.Input(func(e *vecty.Event) {
                    v.roomName = e.Target.Get("value").String()
                })))),
            elem.Paragraph(ons.Button(vecty.Text("Enter"), vecty.Markup(
                vecty.Attribute("modifier", "outline"),
                event.Click(func(_ *vecty.Event) {
                    if v.roomName == "" {
                        v.roomName = base64.RawURLEncoding.EncodeToString(
                            []byte(strconv.FormatInt(time.Now().Unix(), 10)))
                    }
                    dispatcher.Dispatch(actions.EnterRoom{RoomId: v.roomName})
                })))),
            &components.RoomHistoryComponent{})))
    return
}
