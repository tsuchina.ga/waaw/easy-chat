package views

import (
    "github.com/gopherjs/vecty"
    "github.com/gopherjs/vecty/elem"
    "gitlab.com/tsuchina.ga/gonsen"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/components"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/store"
    "net"
    "github.com/gopherjs/websocket"
    "log"
    "encoding/json"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/dispatcher"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/actions"
    "github.com/gopherjs/gopherjs/js"
)

type RoomView struct {
    vecty.Core
    RoomId string
    messages []store.Message
    conn net.Conn
    isConnected bool
}

func (v *RoomView) Render() (body vecty.ComponentOrHTML) {
    if !v.isConnected {
        host := js.Global.Get("location").Get("host").String()
        conn, err := websocket.Dial("ws://" + host + "/ws/" + v.RoomId)
        if err != nil {
            log.Println(err)
            dispatcher.Dispatch(actions.ExitRoom{ToastMessage: "connection failed"})
        } else {
            v.conn = conn
            v.isConnected = true
            go v.readMessage()
        }
    }

    var messages vecty.List
    for _, m := range v.messages {
        messages = append(messages, ons.ListHeader(vecty.Text(
            "[" + m.CreatedAt.Format("2006/01/02 03:04:05") + "] " + m.UserName)))
        messages = append(messages, ons.ListItem(vecty.Text(m.Text)))
    }

    body = elem.Body(
        ons.Page(
            &components.HeaderComponents{IsBackAble: true},
            ons.List(messages),
            &components.MessageFormComponent{Conn: v.conn}))
    return
}

func (v *RoomView) readMessage() {
    for {
        buf := make([]byte, 1024)
        i, err := v.conn.Read(buf)
        if err != nil {
            log.Println(err)
            dispatcher.Dispatch(actions.ExitRoom{ToastMessage: "connection failed"})
            return
        } else {
            var message store.Message
            err = json.Unmarshal(buf[:i], &message)
            if err != nil {
                log.Println("unmarshal error: ", err)
            } else {
                v.messages = append([]store.Message{message}, v.messages...)
                vecty.Rerender(v)
            }
        }
    }
}
