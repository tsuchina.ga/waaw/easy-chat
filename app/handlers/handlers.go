package handlers

import (
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/dispatcher"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/actions"
    "log"
    "github.com/go-humble/router"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/store"
    "encoding/json"
    "time"
    "github.com/gopherjs/gopherjs/js"
    "github.com/go-humble/locstor"
)

var r = router.New()

func init() {
    r.ForceHashURL = true
    dispatcher.Register(handler)
}

func handler(a actions.Action) {
    // log.Println("handle action:", a)
    switch act := a.(type) {
    case actions.EditUserName:
        store.UserName = act.UserName
        locstor.SetItem("user_name", store.UserName)

    case actions.EnterRoom:
        for i, roomId := range store.RoomHistory {
            if roomId == act.RoomId {
                store.RoomHistory = append(store.RoomHistory[:i], store.RoomHistory[i+1:]...)
            }
        }
        store.RoomHistory = append([]string{act.RoomId}, store.RoomHistory...)
        store.RoomHistory = store.RoomHistory[:store.MaxRoomHistory]

        if j, err := json.Marshal(store.RoomHistory); err == nil {
            locstor.SetItem("room_history", string(j))
        }

        r.Navigate("/" + act.RoomId)

    case actions.ExitRoom:
        if act.ToastMessage != "" {
            go delay(500 * time.Millisecond, func() {
                js.Global.Get("ons").Get("notification").Call("toast", act.ToastMessage,
                    map[string]string{"timeout": "2000"})
            })
        }
        r.Navigate("/")

    case actions.SendMessage:
        jsonStr, err := json.Marshal(store.Message{UserName: store.UserName, Text: act.Message, CreatedAt: time.Now()})
        if err != nil {
            log.Println("send message error: ", err)
        } else {
            _, err = act.Conn.Write([]byte(jsonStr))
            if err != nil {
                js.Global.Get("ons").Get("notification").Call("toast", "send failed!!",
                    map[string]string{"timeout": "2000"})
            }
        }

    default:
    }
}

func delay(d time.Duration, f func()) {
    time.Sleep(d)
    f()
}
