package main

import (
    "github.com/go-humble/router"
    "github.com/gopherjs/vecty"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/views"
    _ "gitlab.com/tsuchina.ga/waaw/easy-chat/app/handlers"
    "gitlab.com/tsuchina.ga/waaw/easy-chat/app/store"
)

func main() {
    vecty.SetTitle("easy-chat")

    // routing
    r := router.New()
    r.ForceHashURL = true

    r.HandleFunc("/",
        func(context *router.Context) { vecty.RenderBody(&views.HomeView{UserName: store.UserName}) })
    r.HandleFunc("/{id}",
        func(context *router.Context) { vecty.RenderBody(&views.RoomView{RoomId: context.Params["id"]}) })

    r.Start()
}
