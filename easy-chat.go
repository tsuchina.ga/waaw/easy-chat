package main

import (
    "github.com/gin-gonic/gin"
    "github.com/gin-contrib/gzip"
    "gopkg.in/olahol/melody.v1"
    "log"
    "net/http"
)

func main() {
    log.Println("hello easy-chat")

    r := gin.Default()
    r.Use(gzip.Gzip(gzip.DefaultCompression))

    m := melody.New()

    r.GET("/", func(c *gin.Context) {
        http.ServeFile(c.Writer, c.Request, "assets/index.html")
    })

    r.GET("/ws/:name", func(c *gin.Context) {
        m.HandleRequest(c.Writer, c.Request)
    })

    m.HandleMessageBinary(func(s *melody.Session, msg []byte) {
        m.BroadcastFilter(msg, func(q *melody.Session) bool {
            return q.Request.URL.Path == s.Request.URL.Path
        })
    })

    r.Static("/assets", "./assets")
    r.Run(":18080")
}
