# _WAAW01_ easy-chat

毎週1つWebアプリを作ろうの1回目。

期間は18/05/11 00:00 - 18/05/17 23:59:59


## ゴール

* [x] 簡単なチャットルームの作成。


## サンプル

[http://tcna.ga:18080/](http://tcna.ga:18080)

## 目的

* [x] WebSocketをServerとClientの両方から学ぶ
* [x] Fluxを学ぶ


## 振り返り

0. Idea:

    目的が勉強なのでウェブアプリとしては最悪ですが、間違いなく今後に活かせるものをテーマにできたので選択は間違っていなかった！

0. What went right:

    成功したこと・できたこと
    * websocket(server, client)
    * fluxのアーキテクチャ
    * go言語でのサーバ構築
    * viewの概念(gopherjs)
    * go言語でのクライアント処理(gopherjs)
    * 仮想DOM(vecty, onsenui)

0. What went wrong:

    失敗したこと・できなかったこと
    * ons-navigatorを利用したviewは1つでcomponentを多く用意した形。toastなどを表示中に`RenderBody`が走ると表示していたtoastが消されるということがあり、処理をしながらページの切替が出来ないのは不便。
    * viewのスタックの実装ができなかったので、websocketのconnectionを維持したまま別のページを閲覧することが出来なかった。裏でも動き続けてくれる仕組みを作るならばviewをスタックできないと困る。
    * ssl化
    * 必要なところで非同期に出来ていないことによってレスポンスが遅く感じる画面がある
    * websocketは一方的にメッセージを送りつける仕組みなので、このデータ頂戴！みたいなリクエストは別途apiを用意する必要があり、そこまで手が回らなかった。

0. What I learned:

    学べたこと
    * What went rightで記載した通りのことが学べた。
    * websocketで懸念しておく必要がある接続が切れた場合の動きや、アクティブでない間の挙動などについても気を配る必要があることが分かった。
    * fluxは便利な設計ではあるけれど、小さなプロジェクトでは返って足枷になること
    * dispacherとhundlerとactionを疎結合ながら仲良しにして、routerとcomponentも仲良しにできないか検討したい
    * 自作packageの必要性。vectyを使っているのはまだまだ少数だと思うけど、さらにonsenuiを使っている人はもっと少ないから情報もパッケージも全然ない。ないなら作るはやっぱり大事ね。


## その他

WebServerはGo言語、ClientはGo言語からトランスパイルされたJavascriptで実装する。


## 参考

* [vecty/example/todomvc at master · gopherjs/vecty](https://github.com/gopherjs/vecty/tree/master/example/todomvc)
* [nobonobo/vecty-chatapp: Vecty Samples](https://github.com/nobonobo/vecty-chatapp)
* [Onsen UI 2 Docs - Onsen UI Framework - Onsen UI](https://ja.onsen.io/v2/api/js/)
* [melody/main.go at master · olahol/melody](https://github.com/olahol/melody/blob/master/examples/multichat/main.go)


## つまづき

* [melody](https://github.com/olahol/melody)でのwebsocket Serverでmessageを受信できない

    原因は単純で、`HandleMessageBinary`ではなく`HandleMessage`を使っていたから。
    clientは成功してるのに、serverはうんともすんともいわないから原因特定に手間取った。

* onsenuiのons-inputが複雑？

    ons-input自体はinputではなく、ons-inputの配下にinputが作成される。このinputにpropをつけないといけないのに、それがうまくできないようでヌルポが発生する。
    
    今回は仕方がないのでgetElementByIdをして値を消し込みにいっている。


## 課題


* [x] gopherjs/websocketをつかう

    websocket.Dialで作ったnet.ConnのWriteが動作しない
    -> server側の設定に問題があり、バイナリを受け付けていなかった。

* [x] メッセージ送信後にinputの値を空にする

    vectyのrerender処理で初期化したかったけど上手くいなかった。原因はたぶんons-inputの複雑さにあると思われる。
    vectyで対応できなかったから、gopherjsでgetElementByIdからinputを特定して値を削除。

* [x] net/Connの持ち方

    connを使う箇所が多い。viewでreadし、componentでactionに渡し、actionを通してhandleに渡り、handleがwriteを実行する。
    
    storeにいれるにはactionを発火させないとダメなので、view(1回目) -> action(connectWebsocket) -> handle(connect and save store) -> view
    (2回目 render)ってな感じで自分をレンダリングしなおすというおかしなことになる
    
    もっと共通で簡単にもてるstoreのようなものを用意するか、singletonオブジェクトを別途用意しておくべき
    
    今回は間に合わなかったけど、viewを閉じずに複数持つような工夫ができるようになったときに、viewごとに持っておいたほうがいいので、これは実装しないことを選択。
    むしろ子componentから親componentのメソッドを実行できるようにしたほうが効率いいかも。

* [x] ons.notification.toastの表示

    viewが変わった時にすべてをレンダリングしなおされるようで、toastを出していても消えてしまう。toastだけは消えないようにするか、viewの考え方を変えないといけなさそう。
    
* [ ] viewをバックグラウンドで維持し、非アクティブなビューでもwebsocketの接続を維持してmessageの更新を検知する
    
    できるかは知らんけどできたら便利そう

* [x] app.jsがrenderbodyをするまでの間、ロード中であることが分かる画像か何かを表示する

* [ ] storeの変更を検知してlocalStorageを自動で書き換えに行く

* [x] ons-navigatorの実装も視野に

    色々調査した結果、pageの概念とrouterの概念がうまくマッチしない。その辺のミスマッチを吸収できるパッケージが作れたらいいなって思う。
    
    routerがhistoryを返してくれれば、それをpageのstackに入れるだけでnavigatorを使えないことはなさそうなので、routerに期待する。

* [ ] ssl対応

* [ ] 再アクセス時に数件のログを表示

* [x] より軽量なrouterに変える

    gzipすることでまあまあなパフォーマンスが発揮できたから良しとする。

* [x] gzipしたapp.jsを返す。gopherjsの成果物が大きすぎる

    `github.com/gin-contrib/gzip`すごすぎる。7sくらいかかってたのが600msくらいまで短縮できた。
